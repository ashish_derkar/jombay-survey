import { Component, OnInit } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  // Resolve HTTP using the constructor
  public questions: Array<any>;
  private status;
  private oauthToken;
  private surveyData;
  public activeQuestion;
  public nextHide;
  public previousHide = true;
  constructor(private http: Http) {  }
  private postBody = {
    grant_type: 'password',
    scope: 'user',
    client_id: '4874eafd0f7a240625e59b2b123a142a669923d5b0d31ae8743f6780a95187f5',
    client_secret: '908f6aee4d4cb27782ba55ae0c814bf43419f3220d696206212a29fe3a05cd88',
    auth_token: 'azd4jXWWLagyb9KzgfDJ'
  };
  ngOnInit(): void {
    this.http
      .post('https://simulationapi.ur-nl.com/oauth/token.json', this.postBody)
      .subscribe(
        response => {
          this.oauthToken = response.json();
          const headers = new Headers();
          headers.append('Authorization', 'Bearer ' + this.oauthToken.access_token);
          const options = new RequestOptions({headers: headers});
          this.http.get( 'https://simulationapi.ur-nl.com/users/current.json', options)
            .subscribe(
              data => {
                this.http.get( 'https://simulationapi.ur-nl.com/case_study/companies/58cba141ba169e0eab2657c9/company_case_studies'
                  + '/595c859eba169ec47e4f20d4/user_company_case_studies/595ce021ba169edb9c733e90?include'
                  + '[company_case_study][include]=questions',
                  options)
                  .subscribe(
                    dataJson => {
                      this.surveyData = dataJson.json();
                      this.questions = this.surveyData.user_company_case_study.company_case_study.questions;
                      this.activeQuestion = this.questions[0];
                    },
                    err => this.status.error = JSON.stringify(err)
                  );
              },
              err => this.status.error = JSON.stringify(err)
            );
        }, error => {
        }
      );
  }
  previous() {
    const currentIndex = this.questions.indexOf(this.activeQuestion);
    const newIndex = currentIndex === 0 ? this.questions.length - 1 : currentIndex - 1;
    this.activeQuestion = this.questions[newIndex];
    if (newIndex === 0) {
      this.previousHide = true;
    } else {
      this.previousHide = false;
    }
    this.nextHide = false;
  }

  next() {
    const currentIndex = this.questions.indexOf(this.activeQuestion);
    const newIndex = currentIndex === this.questions.length - 1 ? 0 : currentIndex + 1;
    this.activeQuestion = this.questions[newIndex];
    if (newIndex === this.questions.length - 1) {
      this.nextHide = true;
    } else {
      this.nextHide = false;
    }
    this.previousHide = false;
  }
}
